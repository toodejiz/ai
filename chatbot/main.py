import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--token', dest='token', required=True)
    args = parser.parse_args()
    
    print("Token is {}".format(args.token))

if __name__ == "__main__":
    main()